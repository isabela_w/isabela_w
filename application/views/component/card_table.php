<div class="col">
       <!-- Card Dark -->
<div class="card w-70 p-3">

<!-- Card image -->
<div class="view overlay">
  <img class="card-img-top" src="<?= base_url("assets/img/table.jpg") ?>" alt="Card image cap">
  <a>
    <div class="mask rgba-white-slight"></div>
  </a>
</div>

<!-- Card content -->
<div class="card-body elegant-color white-text rounded-bottom">

  <!-- Social shares button -->
  <a class="activator waves-effect mr-4"><i class="fas fa-share-alt white-text"></i></a>
  <!-- Title -->
  <h4 class="card-title">Table</h4>
  <hr class="hr-light">
  <!-- Text -->
  <p class="card-text white-text mb-4">Aprenda a adicionar uma tabela ao seu site e veja a documentação.</p>
  <!-- Link -->
  <a href="<?= base_url("Template/table") ?>" class="white-text d-flex justify-content-end"><h5>Veja mais <i class="fas fa-angle-double-right"></i></h5></a>

</div>

</div>
<!-- Card Dark -->
    </div>
  </div>
 </div>