
<div class="container mt-5 pt-5">
<h1 class="text-center"> Componentes a serem apresentados: </h1>
<div class="row align-items-end mt-5 pt-5">
    
    <div class="col">
       <!-- Card Dark -->
<div class="card w-70 p-3">

<!-- Card image -->
<div class="view overlay">
  <img class="card-img-top" src="<?= base_url("assets/img/todolist.jpg") ?>" alt="Card image cap">
  <a>
    <div class="mask rgba-white-slight"></div>
  </a>
</div>

<!-- Card content -->
<div class="card-body elegant-color white-text rounded-bottom">

  <!-- Social shares button -->
  <a class="activator waves-effect mr-4"><i class="fas fa-share-alt white-text"></i></a>
  <!-- Title -->
  <h4 class="card-title">To do List</h4>
  <hr class="hr-light">
  <!-- Text -->
  <p class="card-text white-text mb-4">Aprenda a adicionar uma lista de tarefas a fazer no seu site.</p>
  <!-- Link -->
  <a href="<?= base_url("Template/todolist") ?>" class="white-text d-flex justify-content-end"><h5>Leia mais <i class="fas fa-angle-double-right"></i></h5></a>

</div>

</div>
<!-- Card Dark -->
</div>