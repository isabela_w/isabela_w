<hr/>
<div class="container-fluid col-md-6 mb-5 pt-5">
  <p> Estilizar o cabeçalho e a lista </P>
</div>


  <div class="container-fluid col-md-6 ">
    <div class="card">
      <div class="card-body" style="background-color:#f7e8f6;">
        <h5 class="card-title">CSS</h5>
        <p class="card-text ml-5 pl-5" >
        <text class="green-text">/* Inclua o preenchimento e a borda na largura e altura total de um elemento */</text> <br/>
            * { 
              <br/> box-sizing: border-box;<br/> }  <br/>    
              <text class="green-text"> /* Remover margens e preenchimento da lista */</text> <br/> 
          ul { <br/> margin: 0;<br/> padding: 0;<br/> } <br/>
          <text class="green-text"> /* Estilize os itens da lista */ </text>  <br/>
          ul li { <br/> cursor: pointer;<br/> position: relative;<br/> padding: 12px 8px 12px 40px;<br/> background: #eee;<br/> font-size: 18px;<br/> transition: 0.2s; <br/>
            <text class="green-text"> /* Tornar os itens da lista não selecionáveis */ </text> <br/>
            -webkit-user-select: none;<br/> -moz-user-select: none;<br/> -ms-user-select: none;<br/> user-select: none;<br/>   } <br/>
            <text class="green-text"> /* Defina todos os itens da lista ímpares para uma cor diferente (listras de zebra) */ </text> <br/>
           ul li:nth-child(odd) {<br/> background: #f9f9f9;<br/> } <br/>

           <text class="green-text"> /* Cor de fundo mais escura ao passar o mouse */ </text> <br/>
          ul li:hover {<br/> background: #ddd;<br/> } <br/>

          <text class="green-text"> /* Quando clicado, adicione uma cor de fundo e risque o texto */ </text> <br/>
          ul li.checked {<br/> background: #888;<br/> color: #fff;<br/> text-decoration: line-through;<br/> } <br/>

          <text class="green-text"> /*Adicione uma marca "marcada" ao clicar */ </text> <br/>
          ul li.checked::before {<br/>
            content: '';<br/>
            position: absolute;<br/>
            border-color: #fff;<br/>
            border-style: solid;<br/>
            border-width: 0 2px 2px 0;<br/>
            top: 10px;<br/>
            left: 16px;<br/>
            transform: rotate(45deg);<br/>
            height: 15px;<br/>
            width: 7px;<br/>
          } <br/>

          <text class="green-text"> /* Estilo do botão Fechar */ </text> <br/>
          .close {<br/>
            position: absolute;<br/>
            right: 0;<br/>
            top: 0;<br/>
            padding: 12px 16px 12px 16px;<br/>
          }<br/>

          .close:hover {<br/>
            background-color: #f44336;<br/>
            color: white;<br/>
          }<br/>

          <text class="green-text"> /* Estilizar o cabeçalho */ </text> <br/>
          .header {<br/>
            background-color: #f44336;<br/>
            padding: 30px 40px;<br/>
            color: white;<br/>
            text-align: center;<br/>
          }<br/>

          <text class="green-text"> /* Limpar flutuadores após o cabeçalho */ </text> <br/>
          .header:after {<br/>
            content: "";<br/>
            display: table;<br/>
            clear: both;<br/>
          }<br/>

          <text class="green-text"> /* Estilize a entrada */ </text> <br/>
          input {<br/>
            margin: 0;<br/>
            border: none;<br/>
            border-radius: 0;<br/>
            width: 75%;<br/>
            padding: 10px;<br/>
            float: left;<br/>
            font-size: 16px;<br/>
          }<br/>

          <text class="green-text"> /* Estilize o botão "Adicionar" */ </text> <br/>
          .addBtn {<br/>
            padding: 10px;<br/>
            width: 25%;<br/>
            background: #d9d9d9;<br/>
            color: #555;<br/>
            float: left;<br/>
            text-align: center;<br/>
            font-size: 16px;<br/>
            cursor: pointer;<br/>
            transition: 0.3s;<br/>
            border-radius: 0;<br/>
          } <br/>

          .addBtn:hover {<br/>
            background-color: #bbb;<br/>
          } <br/>
        </p>
        </div>
      </div>
    </div>