<hr/>
  
<div class="container-fluid col-md-6 mb-5 pt-5">
  <p>  Adicione JavaScript</p>
</div>

<div class="container-fluid col-md-6">
    <div class="card">
      <div class="card-body" style="background-color:#f7e8f6;">
        <h5 class="card-title">JavaScript</h5>
        <p class="card-text ml-5 pl-5">
        <text class="green-text"> // Crie um botão "fechar" e coloque-o a cada item da lista </text> <br/>
                  var myNodelist = document.getElementsByTagName("LI"); <br/>
                  var i;<br/>
                  for (i = 0; i < myNodelist.length; i++) {<br/>
                    var span = document.createElement("SPAN");<br/>
                    var txt = document.createTextNode("\u00D7");<br/>
                    span.className = "close";<br/>
                    span.appendChild(txt);<br/>
                    myNodelist[i].appendChild(span);<br/>
                  }<br/>

                  <text class="green-text"> // Clique no botão Fechar para ocultar o item da lista atual </text> <br/>
                  var close = document.getElementsByClassName("close");<br/>
                  var i;<br/>
                  for (i = 0; i < close.length; i++) {<br/>
                    close[i].onclick = function() {<br/>
                      var div = this.parentElement;<br/>
                      div.style.display = "none";<br/>
                    }<br/>
                  }<br/>

                  <text class="green-text"> // Adicione um símbolo "marcado" ao clicar em um item da lista </text> <br/>
                  var list = document.querySelector('ul');<br/>
                  list.addEventListener('click', function(ev) {<br/>
                    if (ev.target.tagName === 'LI') {<br/>
                      ev.target.classList.toggle('checked');<br/>
                    }<br/>
                  }, false);<br/>

                  <text class="green-text"> // Crie um novo item na lista ao clicar no botão "Adicionar"</text> <br/>
                  function newElement() {<br/>
                    var li = document.createElement("li");<br/>
                    var inputValue = document.getElementById("myInput").value;<br/>
                    var t = document.createTextNode(inputValue);<br/>
                    li.appendChild(t);<br/>
                    if (inputValue === '') {<br/>
                      alert("You must write something!");<br/>
                    } else {<br/>
                      document.getElementById("myUL").appendChild(li);<br/>
                    }<br/>
                    document.getElementById("myInput").value = "";<br/>

                    var span = document.createElement("SPAN");<br/>
                    var txt = document.createTextNode("\u00D7");<br/>
                    span.className = "close";
                    span.appendChild(txt);<br/>
                    li.appendChild(span);<br/>

                    for (i = 0; i < close.length; i++) {<br/>
                      close[i].onclick = function() {<br/>
                        var div = this.parentElement;<br/>
                        div.style.display = "none";<br/>
            }<br/>
          }<br/>
        } <br/>
        </p>
        
      </div>
    </div>
  </div>
  