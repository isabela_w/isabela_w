<div class="container-fluid col-md-6 ">
  <h1 class="text-center"> Múltiplos Alvos </h1>
    <p> Um link ou um botão podem exibir e ocultar vários elementos.</p>
    <p>Vários usos de botão ou link podem mostrar e ocultar um elemento. </p>
  
      <!-- Collapse buttons -->
<div>
  <a class="btn blue-gradient" data-toggle="collapse" href="#multiCollapseExample1" role="button" aria-expanded="false"
    aria-controls="multiCollapseExample1">Primeiro elemento</a>
  <button class="btn blue-gradient" type="button" data-toggle="collapse" data-target="#multiCollapseExample2"
    aria-expanded="false" aria-controls="multiCollapseExample2">Segundo elemento</button>
  <button class="btn blue-gradient" type="button" data-toggle="collapse" data-target=".multi-collapse"
    aria-expanded="false" aria-controls="multiCollapseExample1 multiCollapseExample2">Ambos elementos</button>
</div>
<!--/ Collapse buttons -->

<!-- Collapsible content -->
<div class="row">
  <div class="col">
    <div class="collapse multi-collapse" id="multiCollapseExample1">
      <div class="card card-body">
        Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. Nihil
        anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident.
      </div>
    </div>
  </div>
  <div class="col">
    <div class="collapse multi-collapse" id="multiCollapseExample2">
      <div class="card card-body">
        Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. Nihil
        anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident.
      </div>
    </div>
  </div>

</div>
<!--/ Collapsible content --> 
<br/>