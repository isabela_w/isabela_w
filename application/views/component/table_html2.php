<div class="card">
      <div class="card-body" style="background-color:#ffe6eb ;">
        <h5 class="card-title">HTML</h5>
        <p class="card-text ml-5 pl-5">
        &lttable class="table"> <br/>
        &ltthead class="black white-text"> <b style="color:#FF1493;"> &lt!-- Definindo a cor de fundo para preto com letra branca --!> </b> <br/>
        &lttr> <br/>
        &ltth scope="col">#&lt/th><br/>
        &ltth scope="col">First&lt/th><br/>
        &ltth scope="col">Last&lt/th><br/>
        &ltth scope="col">Handle&lt/th><br/>
        &lt/tr><br/>
        &lt/thead><br/>
        &lttbody><br/>
        &lttr><br/>
        &ltth scope="row">1&lt/th><br/>
        &lttd>Mark&lt/td><br/>
        &lttd>Otto&lt/td><br/>
        &lttd>@mdo&lt/td><br/>
        &lt/tr><br/>
        &lttr><br/>
        &ltth scope="row">2&lt/th><br/>
        &lttd>Jacob&lt/td><br/>
        &lttd>Thornton&lt/td><br/>
        &lttd>@fat&lt/td><br/>
        &lt/tr><br/>
        &lttr><br/>
        &ltth scope="row">3&lt/th><br/>
        &lttd>Larry&lt/td><br/>
        &lttd>the Bird&lt/td><br/>
        &lttd>@twitter&lt/td><br/>
        &lt/tr><br/>
        &lt/tbody><br/>
              &lt/table> <b style="color:#FF1493;"> &lt!-- Fechamento da primeira tabela --!> </b>  <br/>

              &lttable class="table"><br/>
              &ltthead class="grey lighten-2"> <b style="color:#FF1493;"> &lt!-- Definindo a cor de fundo para cinza --!> </b> <br/>
              &lttr><br/>
              &ltth scope="col">#&lt/th><br/>
              &ltth scope="col">First&lt/th><br/>
              &ltth scope="col">Last&lt/th><br/>
              &ltth scope="col">Handle&lt/th><br/>
              &lt/tr><br/>
              &lt/thead><br/>
              &lttbody><br/>
              &lttr><br/>
              &ltth scope="row">1&lt/th><br/>
              &lttd>Mark&lt/td><br/>
              &lttd>Otto&lt/td><br/>
              &lttd>@mdo&lt/td><br/>
              &lt/tr><br/>
              &lttr><br/>
              &ltth scope="row">2&lt/th><br/>
              &lttd>Jacob&lt/td><br/>
              &lttd>Thornton&lt/td><br/>
              &lttd>@fat&lt/td><br/>
              &lt/tr><br/>
              &lttr><br/>
              &ltth scope="row">3&lt/th><br/>
              &lttd>Larry&lt/td><br/>
              &lttd>the Bird&lt/td><br/>
              &lttd>@twitter&lt/td><br/>
              &lt/tr><br/>
              &lt/tbody><br/>
              &lt/table>
        </p>
      </div>
</div>

</div>
<hr/>