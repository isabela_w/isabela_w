<div class="container-fluid col-md-6 mb-5 pt-5 ">
    <h1 class="text-center"> Collapse </h1>

    <p> É utilizado para mostrar e ocultar um conteúdo. Botões e links são usados para ocultar e exibir o conteúdo. </p>
   
</div>
<hr/>
<div class="container-fluid col-md-6 ">
  <p>Clique nos botões abaixo para exibir e ocultar o conteúdo </p>
  <p>   "collapse" oculta o conteúdo               </p>
  <p>   "collapsing" é aplicado durante transições </p>
  <p>   "collapse.show" mostra conteúdo            </p>
    <!-- Collapse buttons --> </div>
    <div class="container-fluid col-md-6 mb-5 ">
  <a class="btn text-center blue-gradient" data-toggle="collapse" href="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
    Link com href
  </a>
  <button class="btn text-center blue-gradient" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
    Botão com data-target
  </button>
</div>
<!-- / Collapse buttons -->

<!-- Collapsible element -->
<div class="collapse" id="collapseExample">
  <div class="mt-3">
   <p class="text-center"> Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. Nihil anim <br/>
    keffiyeh helvetica,
    craft beer labore wes anderson cred nesciunt sapiente ea proident. </p>
  </div>
</div>
<!-- / Collapsible element -->