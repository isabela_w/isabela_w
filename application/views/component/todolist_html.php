<hr/>
<div class="container-fluid col-md-6 mb-5 pt-5 ">
  <h1 class="text-center"> Como criar uma "To do List" </h1>
  <p >  A "Lista de afazeres" é uma ferramenta para listar suas tarefas, podendo adicionar novas, excluir as existentes e marcar como concluído</p>
</div>

  <div class="container-fluid col-md-6 ">
    <div class="card" >
      <div class="card-body" style="background-color:#f7e8f6;">
        <h5 class="card-title" >HTML</h5>
        <p class="card-text ml-5 pl-5" >
               &ltdiv id="myDIV" class = "header"&gt <br/>
               &lth2&gtMy To Do List&lth2&gt <br/>
               &ltinput type = "text" id="myInput"
               placeholder="Title..."&gt <br/>
               &ltspan onclick="newElement()" class="addBtn"&gtAdd&lt/span&gt <br/>
               &lt/div&gt <br/>

               &ltul id="myUL"&gt <br/>
               &ltli&gtHit the gym&lt/li&gt <br/>
               &ltli class="checked"&gtPay bills&lt/li&gt <br/>
               &ltli&gtMeet George&lt/li&gt <br/>
               &ltli&gtBuy eggs&lt/li&gt <br/>
               &ltli&gtRead a book&lt/li&gt <br/>
               &ltli&gtOrganize office&lt/li&gt <br/>
               &lt/ul&gt 
        </p>
        
      </div>
    </div>
  </div>