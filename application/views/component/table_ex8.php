<div class="container-fluid col-md-6 ">
<h1 class="text-center">Responsive table </h1>
<p> Crie uma tabela responsiva usando a classe "table-responsive" e "text-nowrap" para fazer rolar horizontalmente em dispositivos pequenos
  (abaixo de 768px) </p>
<div class="table-responsive text-nowrap">
  <table class="table">
    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">Heading</th>
        <th scope="col">Heading</th>
        <th scope="col">Heading</th>
        <th scope="col">Heading</th>
        <th scope="col">Heading</th>
        <th scope="col">Heading</th>
        <th scope="col">Heading</th>
        <th scope="col">Heading</th>
        <th scope="col">Heading</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <th scope="row">1</th>
         <?= $html ?>
      </tr>
      <tr>
        <th scope="row">2</th>
        <?= $html ?>
      </tr>
      <tr>
        <th scope="row">3</th>
        <?= $html ?>
      </tr>
    </tbody>
  </table>

</div>