<div class="card">
      <div class="card-body" style="background-color:#ffe6eb;">
        <h5 class="card-title">HTML</h5>
        <p class="card-text ml-5 pl-5">
      <b style="color:#FF1493;"> &lt!-- A tag "&lt table >" é necessária para iniciar a sua tabela !--> </b>
                                                               <br/>
                  &lttable class="table">                      <br/>
      <b style="color:#FF1493;"> &lt!-- A tag "&lt thead >" é usada para definir o cabeçalho da tabela !--> </b> 
                                                               <br/>                              
                     &ltthead>                                 <br/>
      <b style="color:#FF1493;"> &lt!--A tabela é dividida em linhas com a tag "&lt tr >" !--> </b>
                                                               <br/>
                      &lttr>                                   <br/>
       <b style="color:#FF1493;"> &lt!--A tag "&lt th >" é usada para definir uma célula do cabeçalho !--> </b>
                       &ltth scope="col">#&lt/th>              <br/>
                                &ltth scope="col">Col 1&lt/th> <br/>
                                &ltth scope="col">Col 2&lt/th> <br/>
                                &ltth scope="col">Col 3&lt/th> <br/>
                         &lt/tr>                               <br/>
                         &lt/thead>                            <br/>
       <b style="color:#FF1493;"> &lt!--A tag "&lt tbody >" é usada para definir o corpo do cabeçalho !--> </b> 
                                                               <br/>              
                         &lttbody>                             <br/>
                         &lttr>                                <br/>
       <b style="color:#FF1493;"> &lt!--A tag "&lt scope >" é um tributo de acessibilidade, no caso informando que é uma linha "row" !--> </b> 
                                                               <br/>
                         &ltth scope="row">1&lt/th>            <br/>
       <b style="color:#FF1493;"> &lt!--A tag "&lt td >" é usada para definir o conteúdo de uma celula de dados !--> </b> 
                                                               <br/>                         
                                &lttd>Mark&lt/td>              <br/>
                                &lttd>Otto&lt/td>              <br/>
                                &lttd>@mdo&lt/td>              <br/>
                         &lt/tr>                               <br/>
                         &lttr>                                <br/>
                         &ltth scope="row">2&lt/th>            <br/>
                                &lttd>Jacob&lt/td>             <br/>
                                &lttd>Thornton&lt/td>          <br/>
                                &lttd>@fat&lt/td>              <br/>
                         &lt/tr>                               <br/>
                         &lttr>                                <br/>
                         &ltth scope="row">3&lt/th>            <br/>
                                &lttd>Larry&lt/td>             <br/>
                                &lttd>the Bird&lt/td>          <br/>
                                &lttd>@twitter&lt/td>          <br/>
                         &lt/tr>                               <br/>
                         &lt/tbody>                            <br/>
                         &lt/table>                            <br/>
        </p>
      </div>
</div>    
</div>
<hr/>