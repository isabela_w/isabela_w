<div class="card">
      <div class="card-body" style="background-color:#ffe6eb ;">
        <h5 class="card-title">HTML</h5>
        <p class="card-text ml-5 pl-5">
        &lttable class="table table-borderless">  <br/>
        &ltthead>                                 <br/>
        &lttr>                                    <br/>
        &ltth scope="col">#&lt/th>                <br/>
        &ltth scope="col">First&lt/th>            <br/>
        &ltth scope="col">Last&lt/th>             <br/>
        &ltth scope="col">Handle&lt/th>           <br/>
        &lt/tr>                                   <br/>
        &lt/thead>                                <br/>
        &lttbody>                                 <br/>
        &lttr>                                    <br/>
        &ltth scope="row">1&lt/th>                <br/>
        &lttd>Mark&lt/td>                         <br/>
        &lttd>Otto&lt/td>                         <br/>
        &lttd>@mdo&lt/td>                         <br/>
        &lt/tr>                                   <br/>
        &lttr>                                    <br/>
        &ltth scope="row">2&lt/th>                <br/>
        &lttd>Jacob&lt/td>                        <br/>
        &lttd>Thornton&lt/td>                     <br/>
        &lttd>@fat&lt/td>                         <br/>
        &lt/tr>                                   <br/>
        &lttr>                                    <br/>
        &ltth scope="row">3&lt/th>                <br/>
        <b style="color:#FF1493;"> &lt!-- no "colspan" o conteúdo desta linha irá ocupar "2" colunas !--> </b> 
                                                  </br>
        &lttd colspan="2">Larry the Bird&lt/td>   <br/>
        &lttd>@twitter&lt/td>                     <br/>
        &lt/tr>                                   <br/>
        &lt/tbody>                                <br/>
        &lt/table>                                <br/>
        </p>
      </div>
</div>
</div>
<hr/>