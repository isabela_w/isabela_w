<div class="container-fluid col-md-6 mb-5 pt-5 ">
    <h1 class="text-center"> Table </h1>

    <p> Com a tabela é possível exibir dados de uma forma organizada. A tabela é composta por linhas e colunas, sendo possível estilizar
      de diversas formas usando os elementos do Bootstrap e html. </p>
   <h3>Exemplo basico </h3>

   <table class="table">
      <thead>
        <tr>
          <th scope="col">#</th>
          <th scope="col">Col 1</th>
          <th scope="col">Col 2</th>
          <th scope="col">Col 3</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <th scope="row">1</th>
          <td>Mark</td>
          <td>Otto</td>
          <td>@mdo</td>
        </tr>
        <tr>
          <th scope="row">2</th>
          <td>Jacob</td>
          <td>Thornton</td>
          <td>@fat</td>
        </tr>
        <tr>
          <th scope="row">3</th>
          <td>Larry</td>
          <td>the Bird</td>
          <td>@twitter</td>
        </tr>
      </tbody>
    </table>