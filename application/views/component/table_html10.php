<div class="card">
      <div class="card-body" style="background-color:#ffe6eb;">
        <h5 class="card-title">HTML</h5>
        <p class="card-text ml-5 pl-5">
        &lttable class="table table-striped w-auto"><br/>

        &lt!--Table head--><br/>
              &ltthead><br/>
              &lttr><br/>
                  &ltth>#       &lt/th><br/>
                  &ltth>Name    &lt/th><br/>
                  &ltth>Surname &lt/th><br/>
                  &ltth>Country &lt/th><br/>
                  &ltth>City    &lt/th><br/>
                  &ltth>Position&lt/th><br/>
                  &ltth>Age     &lt/th><br/>
                  &lt/tr><br/>
                  &lt/thead><br/>
                  &lt!--Table head--><br/>

                  &lt!--Table body--><br/>
                  &lttbody><br/>
                  &lttr class="table-info"><br/>
                  &ltth scope="row">1&lt/th><br/>
                  &lttd>Kate         &lt/td><br/>
                  &lttd>Moss         &lt/td><br/>
                  &lttd>USA          &lt/td><br/>
                  &lttd>New York City&lt/td><br/>
                  &lttd>Web Designer &lt/td><br/>
                  &lttd>23           &lt/td><br/>
                  &lt/tr><br/>
                  &lttr><br/>
                  &ltth scope="row">2     &lt/th><br/>
                  &lttd>Anna              &lt/td><br/>
                  &lttd>Wintour           &lt/td><br/>
                  &lttd>United Kingdom    &lt/td><br/>
                  &lttd>London            &lt/td><br/>
                  &lttd>Frontend Developer&lt/td><br/>
                  &lttd>36                &lt/td><br/>
                  &lt/tr><br/>
                  &lttr class="table-info"><br/>
                  &ltth scope="row">3&lt/th><br/>
                  &lttd>Tom          &lt/td><br/>
                  &lttd>Bond         &lt/td><br/>
                  &lttd>Spain        &lt/td><br/>
                  &lttd>Madrid       &lt/td><br/>
                  &lttd>Photographer &lt/td><br/>
                  &lttd>25           &lt/td><br/>
                  &lt/tr><br/>
                  &lttr><br/>
                  &ltth scope="row">4  &lt/th><br/>
                  &lttd>Jerry          &lt/td><br/>
                  &lttd>Horwitz        &lt/td><br/>
                  &lttd>Italy          &lt/td><br/>
                  &lttd>Bari           &lt/td><br/>
                  &lttd>Editor-in-chief&lt/td><br/>
                  &lttd>41             &lt/td><br/>
                  &lt/tr><br/>
                  &lttr class="table-info"><br/>
                  &ltth scope="row">5&lt/th><br/>
                  &lttd>Janis      &lt/td><br/>
                  &lttd>Joplin     &lt/td><br/>
                  &lttd>Poland     &lt/td><br/>
                  &lttd>Warsaw     &lt/td><br/>
                  &lttd>Video Maker&lt/td><br/>
                  &lttd>39         &lt/td><br/>
                  &lt/tr><br/>
                  &lttr><br/>
                  &ltth scope="row">6 &lt/th><br/>
                  &lttd>Gary          &lt/td><br/>
                  &lttd>Winogrand     &lt/td><br/>
                  &lttd>Germany       &lt/td><br/>
                  &lttd>Berlin        &lt/td><br/>
                  &lttd>Photographer  &lt/td><br/>
                  &lttd>37            &lt/td><br/>
                  &lt/tr><br/>
                  &lttr class="table-info"><br/>
                  &ltth scope="row">7&lt/th><br/>
                  &lttd>Angie        &lt/td><br/>
                  &lttd>Smith        &lt/td><br/>
                  &lttd>USA          &lt/td><br/>
                  &lttd>San Francisco&lt/td><br/>
                  &lttd>Teacher      &lt/td><br/>
                  &lttd>52           &lt/td><br/>
                &lt/tr><br/>
                &lttr><br/>
                  &ltth scope="row">8&lt/th><br/>
                  &lttd>John&lt/td><br/>
                  &lttd>Mattis&lt/td><br/>
                  &lttd>France&lt/td><br/>
                  &lttd>Paris &lt/td><br/>
                  &lttd>Actor &lt/td><br/>
                  &lttd>28&lt/td><br/>
                  &lt/tr><br/>
                  &lttr class="table-info"><br/>
                  &ltth scope="row">9&lt/th><br/>
                  &lttd>Otto   &lt/td><br/>
                  &lttd>Morris &lt/td><br/>
                  &lttd>Germany&lt/td><br/>
                  &lttd>Munich &lt/td><br/>
                  &lttd>Singer &lt/td><br/>
                  &lttd>35&lt/td><br/>
                  &lt/tr><br/>
                  &lt/tbody><br/>
                  &lt!--Table body--><br/>


                  &lt/table><br/>
                  &lt!--Table--><br/>


        </p>
      </div>
</div>
<br/>