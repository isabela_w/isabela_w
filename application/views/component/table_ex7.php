<hr/>
<div class="container-fluid col-md-6 ">
<h1 class="text-center">Captions </h1>
<p> O "caption" é usado para definir uma legenda para a tabela, auxiliando na acessibilidade para usuários que utilizam leitores de tela saberem
  do que se trata a tabela </p>
<table class="table">
  <caption>Lista de usuarios</caption>
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">First</th>
      <th scope="col">Last</th>
      <th scope="col">Handle</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th scope="row">1</th>
      <td>Mark</td>
      <td>Otto</td>
      <td>@mdo</td>
    </tr>
    <tr>
      <th scope="row">2</th>
      <td>Jacob</td>
      <td>Thornton</td>
      <td>@fat</td>
    </tr>
    <tr>
      <th scope="row">3</th>
      <td>Larry</td>
      <td>the Bird</td>
      <td>@twitter</td>
    </tr>
  </tbody>
</table>