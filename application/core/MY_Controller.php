<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Controller extends CI_Controller{

    public function show($conteudo,$cor)
    {
       // echo '<pre>'.print_r($cor).'</pre>';
       // exit;
        $data['cor'] = $cor; 
        $html = $this->load->view("template/header", null, true);
        $html .= $this->load->view("template/navbar", $data, true);

        $html .= $conteudo;

        $html .= $this->load->view("template/footer", null, true);

        echo $html;
    }

}