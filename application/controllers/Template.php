<?php

class Template extends MY_Controller{

    function __construct(){
        parent::__construct();
        $this->load->model('table_model','table');
      
    }

    public function index(){       
        $html  = $this->load->view("component/card_todolist",null,true);
        $html  .= $this->load->view("component/card_collapse",null,true);
        $html  .= $this->load->view("component/card_table",null,true);
        
        $this->show($html,'#fff');
    }

    public function todolist(){
        $html  = $this->load->view("template/headerList",null,true);
        $html .= $this->load->view("component/todolist_ex",null,true);
        $html .= $this->load->view("component/todolist_html",null,true);
        $html .= $this->load->view("component/todolist_css",null,true);
        $html .= $this->load->view("component/todolist_js",null,true);

        $this->show($html,'#caadde');
    }

    public function collapse(){
        $html = $this->load->view("component/collapse_ex",null,true);
        $html .= $this->load->view("component/collapse_html1",null,true);
        $html .= $this->load->view("component/collapse_ex2",null,true);
        $html .= $this->load->view("component/collapse_html2",null,true);
       
        $this->show($html,'#daf1f9');
    }
    
    public function table(){
        $data['html'] = $this->table->info_table();
        $html = $this->load->view("component/table_intro",null,true);
        $html .= $this->load->view("component/table_html1",null,true);
        $html .= $this->load->view("component/table_ex",null,true);
        $html .= $this->load->view("component/table_html2",null,true);
        $html .= $this->load->view("component/table_ex2",null,true);
        $html .= $this->load->view("component/table_html3",null,true);
        $html .= $this->load->view("component/table_ex3",null,true);
        $html .= $this->load->view("component/table_html4",null,true);
        $html .= $this->load->view("component/table_ex4",null,true);
        $html .= $this->load->view("component/table_html5",null,true);
        $html .= $this->load->view("component/table_ex5",null,true);
        $html .= $this->load->view("component/table_html6",null,true);             
        $html .= $this->load->view("component/table_ex6",null,true);
        $html .= $this->load->view("component/table_html7",null,true);
        $html .= $this->load->view("component/table_ex7",null,true);
        $html .= $this->load->view("component/table_html8",null,true);
        $html .= $this->load->view("component/table_ex8",$data,true);
        $html .= $this->load->view("component/table_html9",null,true);
        $html .= $this->load->view("component/table_ex9",null,true);
        $html .= $this->load->view("component/table_html10",null,true);

        $this->show($html,'#ffcccc');
    }
}



?>